DROP TABLE IF EXISTS user;

CREATE TABLE user (id LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
 user_name VARCHAR(30) NOT NULL,
 email VARCHAR(30) NOT NULL);

CREATE UNIQUE INDEX idx_ue on User(user_name,email);

INSERT INTO User (user_name, email) VALUES ('pavel','pavel@rambler.com');
INSERT INTO User (user_name, email) VALUES ('kolya','kolya@rambler.com');
INSERT INTO User (user_name, email) VALUES ('misha','misha@rambler.com');
INSERT INTO User (user_name, email) VALUES ('yana','yana@rambler.com');

DROP TABLE IF EXISTS account;

CREATE TABLE account (id LONG PRIMARY KEY AUTO_INCREMENT NOT NULL,
user_name VARCHAR(30),
balance DECIMAL(19,4),
currency_code VARCHAR(30)
);

CREATE UNIQUE INDEX idx_acc on account(user_name,currency_code);

INSERT INTO account (user_name,balance,currency_code) VALUES ('pavel',200.0000,'USD');
INSERT INTO account (user_name,balance,currency_code) VALUES ('kolya',400.0000,'USD');
INSERT INTO account (user_name,balance,currency_code) VALUES ('misha',550.0000,'EUR');
INSERT INTO account (user_name,balance,currency_code) VALUES ('yana',120.0000,'EUR');
INSERT INTO account (user_name,balance,currency_code) VALUES ('olya',543.0000,'GBP');
INSERT INTO account (user_name,balance,currency_code) VALUES ('hren',635.0000,'GBP');
