package com.moneytransfer.dao;

import com.moneytransfer.Exceptions.UserException;
import com.moneytransfer.model.User;
import com.moneytransfer.test_utils.DataFitter;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class TestDaoUserImpl {

    private static final Logger log = Logger.getLogger(TestDaoUserImpl.class);
    private static final DataBase dataBase = DataBase.getDataBase(DataBase.H2_DATABASE);

    @BeforeClass
    public static void setup() {
        log.debug("Start to populate H2 by test data");
        DataFitter.populateDate("src/test/resources/scripts.sql");
    }

    @Test
    public void testGetAllUsers() throws UserException {
        assertTrue(4 == dataBase.getUserDao().getAllUsers().size());
    }

    @Test
    public void testGetUserById() throws UserException {
        User actualUser = dataBase.getUserDao().getUserById(3L);
        User expectedUser = new User(3L, "misha", "misha@rambler.com");
        assertTrue(actualUser.equals(expectedUser));
    }

    @Test
    public void testUpdateUser() throws UserException {
        User expectedUser = new User(1L, "sosiska", "sosiska@rambler.ru");
        long count = dataBase.getUserDao().updateUser(1L, expectedUser.getName(), expectedUser.getEmail());
        assertTrue(1L == count);
        assertTrue(expectedUser.equals(dataBase.getUserDao().getUserById(1L)));
    }

    @Test
    public void testCreateUser() throws UserException {
        User newUser = new User(5L, "newComer", "newComer@rambler.com");
        long generatedKey = dataBase.getUserDao().createUser(newUser);
        assertTrue(generatedKey == 5L);
        assertTrue(newUser.equals(dataBase.getUserDao().getUserById(5L)));
    }

    @Test
    public void testDeleteUserById() throws UserException {
        long rowAffected = dataBase.getUserDao().deleteUserById(1L);
        assertTrue(1L == rowAffected);
        assertTrue(dataBase.getUserDao().getUserById(1L) == null);
    }
}
