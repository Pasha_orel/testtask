package com.moneytransfer.dao;

import com.moneytransfer.Exceptions.AccountException;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.Transaction;
import com.moneytransfer.test_utils.DataFitter;
import org.apache.log4j.Logger;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static junit.framework.TestCase.assertTrue;

public class TestDaoAccountImpl {

    private static final Logger log = Logger.getLogger(TestDaoAccountImpl.class);
    private static final DataBase dataBase = DataBase.getDataBase(DataBase.H2_DATABASE);

    @BeforeClass
    public static void setup() {
        log.debug("Start to populate H2 by test data");
        DataFitter.populateDate("src/test/resources/scripts.sql");
    }

    @Test
    public void testGetAllAccounts() throws AccountException{
        /*we need to populate it again because it depends on other test methods*/
        DataFitter.populateDate("src/test/resources/scripts.sql");
        assertTrue(6 == dataBase.getAccauntDao().getAllAccounts().size());
    }

    @Test
    public void testCreateAccount() throws AccountException {
        Account newAccount = new Account(7L,"newComer",new BigDecimal("1000.0000"), "USD");
        long accountId = dataBase.getAccauntDao().createAccount(newAccount);
        assertTrue(accountId == 7L);
        assertTrue(newAccount.equals(dataBase.getAccauntDao().getAccountById(7L)));
    }

    @Test
    public void testDeleteAccount() throws AccountException{
        long rowCount = dataBase.getAccauntDao().deleteAccountById(1L);
        assertTrue(rowCount == 1L);
        assertTrue(dataBase.getAccauntDao().getAccountById(1L) == null);
    }

    @Test
    public void testGetAccountById() throws AccountException{
        Account expectedAccount = new Account(2L, "kolya", new BigDecimal("400.0000"), "USD");
        Account actualAccount = dataBase.getAccauntDao().getAccountById(2L);
        assertTrue(actualAccount.equals(expectedAccount));
    }

    @Test
    public void testUpdateBalanceById() throws AccountException{
        /*we need to populate it again because it depends on other test methods*/
        DataFitter.populateDate("src/test/resources/scripts.sql");

        BigDecimal deltaDeposit = new BigDecimal(50).setScale(4, RoundingMode.HALF_EVEN);
        BigDecimal afterDeposit = new BigDecimal(250).setScale(4, RoundingMode.HALF_EVEN);
        long rowsUpdated = dataBase.getAccauntDao().updateBalanceById(1L, deltaDeposit);
        assertTrue(rowsUpdated == 1L);
        assertTrue(dataBase.getAccauntDao().getAccountById(1L).getBalance().equals(afterDeposit));
        BigDecimal deltaWithDraw = new BigDecimal(-50).setScale(4, RoundingMode.HALF_EVEN);
        BigDecimal afterWithDraw = new BigDecimal(200).setScale(4, RoundingMode.HALF_EVEN);
        long rowsUpdatedW = dataBase.getAccauntDao().updateBalanceById(1L, deltaWithDraw);
        assertTrue(rowsUpdatedW == 1L);
        assertTrue(dataBase.getAccauntDao().getAccountById(1L).getBalance().equals(afterWithDraw));
    }

    @Test
    public void testTransferBalance() throws AccountException{
        /*we need to populate it again because it depends on other test methods*/
        DataFitter.populateDate("src/test/resources/scripts.sql");

        Transaction transaction = new Transaction("USD", new BigDecimal("100.0000"),1L,2L);
        int rowCount = dataBase.getAccauntDao().transferBalance(transaction);
        assertTrue(rowCount == 2);
        assertTrue(dataBase.getAccauntDao().getAccountById(1L).getBalance().equals(new BigDecimal("100.0000")));
        assertTrue(dataBase.getAccauntDao().getAccountById(2L).getBalance().equals(new BigDecimal("500.0000")));
    }
}
