package com.moneytransfer.test_utils;

import com.moneytransfer.dao.impl.DataBaseH2Impl;
import org.apache.log4j.Logger;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.SQLException;

public class DataFitter {

    private static final Logger log = Logger.getLogger(DataFitter.class);

    public static void populateDate(String resource) {
        try (Connection con = DataBaseH2Impl.getConnection()) {
            RunScript.execute(con, new FileReader(resource));
        } catch (SQLException e) {
            log.error("connection was not initialized");
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            log.error("scripts.sql file was not found");
            e.printStackTrace();
        }
    }
}
