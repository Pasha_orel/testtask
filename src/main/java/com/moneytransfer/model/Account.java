package com.moneytransfer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.Objects;

public class Account {

    @JsonIgnore
    private long id;
    @JsonProperty(required = true)
    private String name;
    @JsonProperty(required = true)
    private BigDecimal balance;
    @JsonProperty(required = true)
    private String currencyCode;

    public Account(){}

    public Account(String name, BigDecimal balance, String currencyCode) {
        this.name = name;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    public Account(long id, String name, BigDecimal balance, String currencyCode) {
        this.id = id;
        this.name = name;
        this.balance = balance;
        this.currencyCode = currencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return id == account.id &&
                Objects.equals(name, account.name) &&
                Objects.equals(balance, account.balance) &&
                Objects.equals(currencyCode, account.currencyCode);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, balance, currencyCode);
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", balance=" + balance +
                ", currencyCode='" + currencyCode + '\'' +
                '}';
    }
}
