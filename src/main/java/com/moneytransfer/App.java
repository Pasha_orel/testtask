package com.moneytransfer;

import com.moneytransfer.services.*;
import org.apache.log4j.Logger;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class App {

    private static Logger log = Logger.getLogger(App.class);

    public static void main(String[] args) throws Exception {

        log.info("App is running");
        Server server = new Server(8080);
        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.setContextPath("/");
        server.setHandler(context);
        ServletHolder servletHolder = context.addServlet(ServletContainer.class, "/*");

        servletHolder.setInitParameter("jersey.config.server.provider.classnames",
                UserService.class.getCanonicalName() + "," + AccountService.class.getCanonicalName() + ","
                        + AccountExceptionMapper.class.getCanonicalName() + ","
                        + UserExceptionMapper.class.getCanonicalName() + ","
                        + TransactionService.class.getCanonicalName());

        try {
            server.start();
            server.join();
        } finally {
            server.destroy();
        }

    }


}
