package com.moneytransfer.utils;

public class SqlQueries {

    /*sql for users*/
    public final static String GET_ALL_USERS = "SELECT * FROM user";
    public final static String GET_USER_BY_ID = "SELECT * FROM user WHERE id= ? ";
    public final static String DELETE_USER_BY_ID = "DELETE FROM user WHERE id= ? ";
    public final static String UPDATE_USER_BY_ID = "UPDATE user SET user_name = ?, email = ? WHERE id= ? ";
    public final static String INSERT_USER = "INSERT INTO user (user_name, email) VALUES (?, ?)";

    /*sql for accounts*/
    public final static String CREATE_ACCOUNT = "INSERT INTO account (user_name, balance, currency_code) VALUES (?, ?, ?)";
    public final static String DELETE_ACCOUNT_BY_ID = "DELETE FROM account WHERE id = ?";
    public final static String GET_ACCOUNT_BY_ID = "SELECT * FROM account WHERE id = ? ";
    public final static String UPDATE_ACCOUNT_BALANCE = "UPDATE account SET balance = ? WHERE id = ? ";
    public final static String GET_ALL_ACCOUNTS = "SELECT * FROM account";
    public final static String LOCK_ACCOUNT_BY_ID = "SELECT * FROM account WHERE id = ? FOR UPDATE";
}
