package com.moneytransfer.utils;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyValue {

    private static final Logger log = Logger.getLogger(PropertyValue.class);
    public static Properties props = new Properties();

    static {
        load("app.properties");
    }

    private static void load(String propFileName) {
        try (InputStream is = ClassLoader.getSystemResourceAsStream(propFileName)) {
            props.load(is);
        } catch (IOException ex) {
            log.error("Logger was not initialized!!!");
        }
    }
}
