package com.moneytransfer.dao;

import com.moneytransfer.dao.impl.DataBaseH2Impl;

public abstract class DataBase {

    public static final int H2_DATABASE = 1;

    public abstract DaoUser getUserDao();

    public abstract DaoAccount getAccauntDao();

    public static DataBase getDataBase(int code) {
        switch (code) {
            case H2_DATABASE:
                return new DataBaseH2Impl();
            default:
                //could be some others databases
                return new DataBaseH2Impl();
        }
    }

}
