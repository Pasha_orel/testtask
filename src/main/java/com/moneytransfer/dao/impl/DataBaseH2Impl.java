package com.moneytransfer.dao.impl;

import com.moneytransfer.dao.DaoAccount;
import com.moneytransfer.dao.DaoUser;
import com.moneytransfer.dao.DataBase;
import com.moneytransfer.utils.PropertyValue;
import org.apache.commons.dbutils.DbUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseH2Impl extends DataBase {

    private static final String H2_DRIVER = "org.h2.Driver";
    private DaoAccount daoAccount = new DaoAccountImpl();
    private DaoUser daoUser = new DaoUserImpl();

    static {
        DbUtils.loadDriver(H2_DRIVER);
    }

    public static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(PropertyValue.props.getProperty("db_url"),
                PropertyValue.props.getProperty("db_user"),
                PropertyValue.props.getProperty("db_password"));
    }

    @Override
    public DaoUser getUserDao() {
       return daoUser;
    }

    @Override
    public DaoAccount getAccauntDao() {
        return daoAccount;
    }
}
