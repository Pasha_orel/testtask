package com.moneytransfer.dao.impl;

import com.moneytransfer.Exceptions.AccountException;
import com.moneytransfer.dao.DaoAccount;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.Transaction;
import com.moneytransfer.utils.CurrencyTracker;
import com.moneytransfer.utils.SqlQueries;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.math.BigDecimal;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoAccountImpl implements DaoAccount {

    private static final Logger log = Logger.getLogger(DaoAccountImpl.class);

    @Override
    public long createAccount(Account account) throws AccountException {
        ResultSet result = null;
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.CREATE_ACCOUNT, Statement.RETURN_GENERATED_KEYS)) {

            stm.setString(1, account.getName());
            stm.setBigDecimal(2, account.getBalance());
            stm.setString(3, account.getCurrencyCode());
            int affectedRows = stm.executeUpdate();
            if (affectedRows == 0) {
                log.error("createAccount(): Creating account failed, no rows affected.");
                throw new AccountException("Account Cannot be created");
            }
            result = stm.getGeneratedKeys();
            if (result.next()) {
                return result.getLong(1);
            } else {
                log.error("Creating account failed, no ID obtained.");
                throw new AccountException("Account Cannot be created");
            }
        } catch (SQLException e) {
            //logger
            e.printStackTrace();
            throw new AccountException("Smth went wrong with user");
        } finally {
            DbUtils.closeQuietly(result);
        }
    }

    @Override
    public long deleteAccountById(long id) throws AccountException {
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.DELETE_ACCOUNT_BY_ID)) {
            stm.setLong(1, id);
            return stm.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new AccountException("Smth wrong!");
        }
    }

    @Override
    public Account getAccountById(long id) throws AccountException {
        ResultSet result;
        Account account = null;
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.GET_ACCOUNT_BY_ID)) {
            stm.setLong(1, id);
            result = stm.executeQuery();
            if (result.next()) {
                account = new Account(result.getLong("id"),
                        result.getString("user_name"),
                        result.getBigDecimal("balance"),
                        result.getString("currency_code"));
            }
            return account;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new AccountException("Account does not exist");
        }
    }

    @Override
    public List<Account> getAllAccounts() throws AccountException {
        ResultSet result;
        Account account;
        List<Account> list = new ArrayList<>();
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.GET_ALL_ACCOUNTS)) {
            result = stm.executeQuery();
            while (result.next()) {
                account = new Account(result.getString("user_name"),
                        result.getBigDecimal("balance"),
                        result.getString("currency_code"));
                list.add(account);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new AccountException("Accounts do not exist");
        }
    }

    @Override
    public long updateBalanceById(long id, BigDecimal newBalance) throws AccountException {
        Connection con = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        Account targetAccount = null;
        int updateCount = -1;
        try {
            con = DataBaseH2Impl.getConnection();
            con.setAutoCommit(false);
            // lock account for writing:
            lockStmt = con.prepareStatement(SqlQueries.LOCK_ACCOUNT_BY_ID);
            lockStmt.setLong(1, id);
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                targetAccount = new Account(rs.getLong("id"), rs.getString("user_name"),
                        rs.getBigDecimal("balance"), rs.getString("currency_code"));
            }

            if (targetAccount == null) {
                throw new AccountException("updateAccountBalance(): fail to lock account : " + id);
            }
            // update account upon success locking
            BigDecimal balance = targetAccount.getBalance().add(newBalance);
            if (balance.compareTo(CurrencyTracker.zeroAmount) < 0) {
                throw new AccountException("Not sufficient Fund for account: " + id);
            }

            updateStmt = con.prepareStatement(SqlQueries.UPDATE_ACCOUNT_BALANCE);
            updateStmt.setBigDecimal(1, balance);
            updateStmt.setLong(2, id);
            updateCount = updateStmt.executeUpdate();
            con.commit();
            if (log.isDebugEnabled())
                log.debug("New Balance after Update: " + targetAccount);
            return updateCount;
        } catch (SQLException se) {
            // rollback transaction if exception occurs
             log.error("updateAccountBalance(): User Transaction Failed, rollback initiated for: " + id, se);
            try {
                if (con != null)
                    con.rollback();
            } catch (SQLException re) {
                throw new AccountException("Fail to rollback transaction", re);
            }
        } finally {
            DbUtils.closeQuietly(con);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
        return updateCount;
    }

    @Override
    public int transferBalance(Transaction transaction) throws AccountException {
        int result = -1;
        Connection conn = null;
        PreparedStatement lockStmt = null;
        PreparedStatement updateStmt = null;
        ResultSet rs = null;
        Account fromAccount = null;
        Account toAccount = null;

        try {
            conn = DataBaseH2Impl.getConnection();
            conn.setAutoCommit(false);
            // lock the credit and debit account for writing:
            lockStmt = conn.prepareStatement(SqlQueries.LOCK_ACCOUNT_BY_ID);
            lockStmt.setLong(1, transaction.getFromAccountId());
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                fromAccount = new Account(rs.getLong("id"), rs.getString("user_name"),
                        rs.getBigDecimal("balance"), rs.getString("currency_code"));
                if (log.isDebugEnabled())
                    log.debug("transferAccountBalance from Account: " + fromAccount);
            }
            lockStmt = conn.prepareStatement(SqlQueries.LOCK_ACCOUNT_BY_ID);
            lockStmt.setLong(1, transaction.getToAccountId());
            rs = lockStmt.executeQuery();
            if (rs.next()) {
                toAccount = new Account(rs.getLong("id"), rs.getString("user_name"), rs.getBigDecimal("balance"),
                        rs.getString("currency_code"));
                if (log.isDebugEnabled())
                    log.debug("transferAccountBalance to Account: " + toAccount);
            }

            // check locking status
            if (fromAccount == null || toAccount == null) {
                throw new AccountException("Fail to lock both accounts for write");
            }

            // check transaction currency
            if (!fromAccount.getCurrencyCode().equals(transaction.getCurrencyCode())) {
                throw new AccountException(
                        "Fail to transfer Fund, transaction ccy are different from source/destination");
            }

            // check ccy is the same for both accounts
            if (!fromAccount.getCurrencyCode().equals(toAccount.getCurrencyCode())) {
                throw new AccountException(
                        "Fail to transfer Fund, the source and destination account are in different currency");
            }

            // check enough fund in source account
            BigDecimal fromAccountLeftOver = fromAccount.getBalance().subtract(transaction.getAmount());
            if (fromAccountLeftOver.compareTo(CurrencyTracker.zeroAmount) < 0) {
                throw new AccountException("Not enough Fund from source Account ");
            }
            // proceed with update
            updateStmt = conn.prepareStatement(SqlQueries.UPDATE_ACCOUNT_BALANCE);
            updateStmt.setBigDecimal(1, fromAccountLeftOver);
            updateStmt.setLong(2, transaction.getFromAccountId());
            updateStmt.addBatch();
            updateStmt.setBigDecimal(1, toAccount.getBalance().add(transaction.getAmount()));
            updateStmt.setLong(2, transaction.getToAccountId());
            updateStmt.addBatch();
            int[] rowsUpdated = updateStmt.executeBatch();
            result = rowsUpdated[0] + rowsUpdated[1];
            if (log.isDebugEnabled()) {
                log.debug("Number of rows updated for the transfer : " + result);
            }
            // If there is no error, commit the transaction
            conn.commit();
        } catch (SQLException se) {
            // rollback transaction if exception occurs
            log.error("transferAccountBalance(): User Transaction Failed, rollback initiated for: " + transaction,
                    se);
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException re) {
                throw new AccountException("Fail to rollback transaction", re);
            }
        } finally {
            DbUtils.closeQuietly(conn);
            DbUtils.closeQuietly(rs);
            DbUtils.closeQuietly(lockStmt);
            DbUtils.closeQuietly(updateStmt);
        }
        return result;
    }
}
