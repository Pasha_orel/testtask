package com.moneytransfer.dao.impl;

import com.moneytransfer.Exceptions.UserException;
import com.moneytransfer.dao.DaoUser;
import com.moneytransfer.model.User;
import com.moneytransfer.utils.SqlQueries;
import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DaoUserImpl implements DaoUser {

    private static final Logger log = Logger.getLogger(DaoUserImpl.class);

    @Override
    public long createUser(User user) throws UserException {
        ResultSet result = null;
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {

            stm.setString(1, user.getName());
            stm.setString(2, user.getEmail());
            int affectedRows = stm.executeUpdate();
            if (affectedRows == 0) {
                log.error("insertUser(): Creating user failed, no rows affected." + user);
                throw new UserException("Users Cannot be created");
            }
            result = stm.getGeneratedKeys();
            if (result.next()) {
                return result.getLong(1);
            } else {
                log.error("insertUser():  Creating user failed, no ID obtained." + user);
                throw new UserException("Users Cannot be created");
            }
        } catch (SQLException e) {
            //logger
            e.printStackTrace();
            throw new UserException("Smth went wrong with user");
        } finally {
            DbUtils.closeQuietly(result);
        }
    }

    @Override
    public long deleteUserById(long id) throws UserException {
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.DELETE_USER_BY_ID)) {
            stm.setLong(1, id);
            return stm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserException("Smth wrong!");
        }
    }

    @Override
    public long updateUser(long id, String name, String email) throws UserException {
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.UPDATE_USER_BY_ID)) {
            stm.setString(1, name);
            stm.setString(2, email);
            stm.setLong(3, id);
            return stm.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserException("Smth wrong!");
        }
    }

    @Override
    public User getUserById(long id) throws UserException {
        ResultSet result = null;
        User user = null;
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.GET_USER_BY_ID)) {
            stm.setLong(1, id);
            result = stm.executeQuery();
            if (result.next()) {
                user = new User(result.getLong("id"),
                        result.getString("user_name"),
                        result.getString("email"));
            }
            return user;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserException("User does not exist");
        }
    }

    @Override
    public List<User> getAllUsers() throws UserException {
        ResultSet result = null;
        User user = null;
        List<User> list = new ArrayList<>();
        try (Connection con = DataBaseH2Impl.getConnection();
             PreparedStatement stm = con.prepareStatement(SqlQueries.GET_ALL_USERS)) {
            result = stm.executeQuery();
            while (result.next()) {
                user = new User(result.getLong("id"),
                        result.getString("user_name"),
                        result.getString("email"));
                list.add(user);
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new UserException("Users do not exist");
        }
    }
}
