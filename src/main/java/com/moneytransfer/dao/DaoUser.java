package com.moneytransfer.dao;

import com.moneytransfer.Exceptions.UserException;
import com.moneytransfer.model.User;

import java.util.List;

public interface DaoUser {

    long createUser(User user) throws UserException;
    long deleteUserById(long id) throws UserException;
    long updateUser(long id, String name, String email) throws UserException;
    User getUserById(long id) throws UserException;
    List<User> getAllUsers() throws UserException;

}
