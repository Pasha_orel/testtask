package com.moneytransfer.dao;

import com.moneytransfer.Exceptions.AccountException;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.Transaction;

import java.math.BigDecimal;
import java.util.List;

public interface DaoAccount {

    long createAccount(Account account) throws AccountException;
    long deleteAccountById(long id) throws AccountException;
    Account getAccountById(long id) throws AccountException;
    List<Account> getAllAccounts() throws AccountException;
    long updateBalanceById(long id, BigDecimal newBalance) throws AccountException;
    int transferBalance(Transaction transaction) throws AccountException;


}
