package com.moneytransfer.services;

import com.moneytransfer.Exceptions.ErrorResponse;
import com.moneytransfer.Exceptions.UserException;
import org.apache.log4j.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class UserExceptionMapper implements ExceptionMapper<UserException> {

    private static Logger log = Logger.getLogger(UserExceptionMapper.class);

    public UserExceptionMapper() {
    }

    public Response toResponse(UserException daoException) {
        if (log.isDebugEnabled()) {
            log.debug("Mapping exception to Response....");
        }
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(daoException.getMessage());

        // return internal server error for DAO exceptions
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(errorResponse).type(MediaType.APPLICATION_JSON).build();


    }

}
