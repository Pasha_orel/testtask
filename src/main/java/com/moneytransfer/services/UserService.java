package com.moneytransfer.services;

import com.moneytransfer.Exceptions.UserException;
import com.moneytransfer.dao.DataBase;
import com.moneytransfer.model.User;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
public class UserService {

    private static Logger log = Logger.getLogger(UserService.class);
    private final DataBase daoFactory = DataBase.getDataBase(DataBase.H2_DATABASE);


    /**
     * Find by all
     *
     * @return
     * @throws UserException
     */
    @GET
    @Path("/all")
    public List<User> getAllUsers() throws UserException {
        return daoFactory.getUserDao().getAllUsers();
    }

    /**
     * Create User
     *
     * @param user
     * @return
     * @throws UserException
     */
    @POST
    @Path("/create")
    public User createUser(User user) throws UserException {
        if (daoFactory.getUserDao().getUserById(user.getId()) != null) {
            throw new WebApplicationException("User already exist", Response.Status.BAD_REQUEST);
        }
        final long uId = daoFactory.getUserDao().createUser(user);
        return daoFactory.getUserDao().getUserById(uId);
    }

    /**
     * Find by User Id
     *
     * @param id
     * @param user
     * @return
     * @throws UserException
     */
    @PUT
    @Path("/{userId}")
    public Response updateUser(@PathParam("userId") long id, User user) throws UserException {
        final long updateCount = daoFactory.getUserDao().updateUser(id, user.getName(), user.getEmail());
        if (updateCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

    /**
     * Delete by User Id
     *
     * @param id
     * @return
     * @throws UserException
     */
    @DELETE
    @Path("/{id}")
    public Response deleteUser(@PathParam("id") long id) throws UserException {
        long deleteCount = daoFactory.getUserDao().deleteUserById(id);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
