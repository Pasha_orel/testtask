package com.moneytransfer.services;

import com.moneytransfer.Exceptions.AccountException;
import com.moneytransfer.dao.DataBase;
import com.moneytransfer.model.Transaction;
import com.moneytransfer.utils.CurrencyTracker;
import org.apache.log4j.Logger;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/transaction")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionService {

    private static final Logger log = Logger.getLogger(TransactionService.class);
    private final DataBase daoFactory = DataBase.getDataBase(DataBase.H2_DATABASE);

    /**
     * Transfer fund between two accounts.
     * @param transaction
     * @return
     * @throws AccountException
     */
    @POST
    public Response transferFund(Transaction transaction) throws AccountException {

        String currency = transaction.getCurrencyCode();
        if (CurrencyTracker.INSTANCE.validateCcyCode(currency)) {
            int updateCount = daoFactory.getAccauntDao().transferBalance(transaction);
            if (updateCount == 2) {
                return Response.status(Response.Status.OK).build();
            } else {
                // transaction failed
                throw new WebApplicationException("Transaction failed", Response.Status.BAD_REQUEST);
            }

        } else {
            throw new WebApplicationException("Currency Code Invalid ", Response.Status.BAD_REQUEST);
        }

    }

}
