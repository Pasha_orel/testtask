package com.moneytransfer.services;

import com.moneytransfer.Exceptions.AccountException;
import com.moneytransfer.dao.DataBase;
import com.moneytransfer.model.Account;
import org.apache.log4j.Logger;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

@Path("/account")
@Produces(MediaType.APPLICATION_JSON)
public class AccountService {

    private static Logger log = Logger.getLogger(AccountService.class);
    private final DataBase daoFactory = DataBase.getDataBase(DataBase.H2_DATABASE);

    /**
     * Find all accounts
     *
     * @return
     * @throws AccountException
     */
    @GET
    @Path("/all")
    public List<Account> getAllAccounts() throws AccountException {
        return daoFactory.getAccauntDao().getAllAccounts();
    }

    /**
     * Find by account id
     *
     * @param id
     * @return
     * @throws AccountException
     */
    @GET
    @Path("/{id}")
    public Account getAccount(@PathParam("id") long id) throws AccountException {
        return daoFactory.getAccauntDao().getAccountById(id);
    }

    /**
     * Find balance by account Id
     *
     * @param id
     * @return
     * @throws AccountException
     */
    @GET
    @Path("/{id}/balance")
    public BigDecimal getBalance(@PathParam("id") long id) throws AccountException {
        final Account account = daoFactory.getAccauntDao().getAccountById(id);

        if (account == null) {
            throw new WebApplicationException("Account not found", Response.Status.NOT_FOUND);
        }
        return account.getBalance();
    }

    /**
     * Create Account
     *
     * @param account
     * @return
     * @throws AccountException
     */
    @PUT
    @Path("/create")
    public Account createAccount(Account account) throws AccountException {
        final long accountId = daoFactory.getAccauntDao().createAccount(account);
        return daoFactory.getAccauntDao().getAccountById(accountId);
    }

    /**
     * Deposit amount by account Id
     *
     * @param id
     * @param amount
     * @return
     * @throws AccountException
     */
    @PUT
    @Path("/{id}/deposit/{amount}")
    public Account deposit(@PathParam("accountId") long id, @PathParam("amount") BigDecimal amount) throws AccountException {

/*        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }*/

        daoFactory.getAccauntDao().updateBalanceById(id, amount.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccauntDao().getAccountById(id);
    }

    /**
     * Withdraw amount by account Id
     *
     * @param id
     * @param amount
     * @return
     * @throws AccountException
     */
    @PUT
    @Path("/{id}/withdraw/{amount}")
    public Account withdraw(@PathParam("accountId") long id, @PathParam("amount") BigDecimal amount) throws AccountException {

/*        if (amount.compareTo(MoneyUtil.zeroAmount) <=0){
            throw new WebApplicationException("Invalid Deposit amount", Response.Status.BAD_REQUEST);
        }*/
        BigDecimal delta = amount.negate();
        if (log.isDebugEnabled())
            log.debug("Withdraw service: delta change to account  " + delta + " Account ID = " + id);
        daoFactory.getAccauntDao().updateBalanceById(id, delta.setScale(4, RoundingMode.HALF_EVEN));
        return daoFactory.getAccauntDao().getAccountById(id);
    }


    /**
     * Delete amount by account Id
     *
     * @param id
     * @return
     * @throws AccountException
     */
    @DELETE
    @Path("/{id}")
    public Response deleteAccount(@PathParam("id") long id) throws AccountException {
        long deleteCount = daoFactory.getAccauntDao().deleteAccountById(id);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }
}
