__**APP FOR MONEY TRANSFERRING**__

Java RESTfull API for money-transferring between accounts

_Technologies:_
H2_Datastore (inMemory mode)
Jetty Container
Apache HTTP Client
JAX-RS API

All tests are populated by test-data.
You can find tests-examples in 
scripts.sql


_How to run the main app:_
javac com.moneytransfer.App
java com.moneytransfer.App
